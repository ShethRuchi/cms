// Module
const Router = require('router');
const bodyParser = require('body-parser');
const router = Router();
router.use(bodyParser.json());

// Files
const registerController = require('../controllers/auth/register.controller');

/**
 * Routes of Project
 */

// Health check
router.route('/health-check',).get((req, res) => {
    const response = {
        status: 200,
        message: "PING",
        data: {}
    };
    res.write(JSON.stringify(response));
    res.end();
});

/*
* Auth Routes
*/
router.route('/register-user',).post(
    registerController.registerUser
);


module.exports = router;
