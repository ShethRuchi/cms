class ResponseDispatcher {
    dispatchSuccess(res, data, message) {
        res.setHeader('Content-Type', 'application/json');
        res.setHeader('charset', 'utf-8');
        res.writeHead(200);
        res.write(this.jsonResponse(data, message, 200));
        res.end();
    }

    dispatchError(res, exception) {
        console.log('exception =>', exception);
        res.setHeader('Content-Type', 'application/json');
        res.setHeader('charset', 'utf-8');
        res.writeHead();
        res.write(this.jsonResponse(true, null, errorData.message, statusCode));
        res.end();
    }

    jsonResponse(data = {}, message = '', code = 200) {
        const response = {
            code,
            message,
            data
        };
        return (JSON.stringify(response));
    };
}

const responseDispatcher = new ResponseDispatcher();
module.exports = responseDispatcher;
