const mysql = require('mysql');

class UserRepository {

    async storeUser(input) {
        const dbConnection = mysql.createConnection({
            host: process.env.DB_HOST,
            port: process.env.DB_PORT,
            user: process.env.DB_USERNAME,
            password: process.env.DB_PASSWORD,
            database: process.env.DB_DATABASE
        });

        const promise = new Promise((resolve, reject) => {
            dbConnection.connect((error) => {
                if (error) {
                    reject(new Error(error));
                }
                resolve('connected');
            });
        });

        const query = `insert into users (username, password, first_name, last_name, contact_number, email) 
                            VALUES(
                                '${input['username']}',
                                '${input['password']}',
                                '${input['first_name']}',
                                '${input['last_name']}',
                                ${input['contact_number']},
                                '${input['email']}'
                            )`;

        const userId = await new Promise((resolve, reject) => {
            dbConnection.query(query, [], (err, results) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(results.insertId);
                }
            });
        });

        const userQuery = `SELECT * FROM users WHERE id = ${userId}`;

        return new Promise((resolve, reject) => {
            dbConnection.query(userQuery, [], (err, results) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(results[0]);
                }
            });
        });
    }
}

const userRepository = new UserRepository();
module.exports = userRepository;
