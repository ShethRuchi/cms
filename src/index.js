const http = require('http');
const Router = require('router');
const finalhandler = require('finalhandler');

const routes = require('./routes');
const router = Router();
router.use('/', routes);

const server = http.createServer((req, res) => {
    router(req, res, finalhandler(req, res));
});

server.listen(9003, () => {
    console.log("server started on port 9003");
});

