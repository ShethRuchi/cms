const responseDispatcher = require('../../lib/response-dispatcher');
const userRepository = require('../../repository/user-repository');

class RegisterController {

    /**
     * Register an User
     * @param req
     * @param res
     */
    async registerUser(req, res) {
        try {
            const input = req.body;
            const response = await userRepository.storeUser(input);
            return responseDispatcher.dispatchSuccess(res, response, "User registered successfully");
        } catch (e) {
            console.log('inside catch');
            return responseDispatcher.dispatchError(res, e);
        }

    }
}

const registerController = new RegisterController();
module.exports = registerController;
